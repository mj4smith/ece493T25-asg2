import numpy as np
import pandas as pd


class rlalgorithm:
    def __init__(self, actions, learning_rate=0.01, reward_decay=0.9, e_greedy=0.1):
        self.actions = actions
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.q_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.display_name = "On Policy SARSA"

    '''States are dynamically added to the Q(S,A) table as they are encountered'''
    def check_state_exist(self, state):
        if state not in self.q_table.index:
            # append new state to q table
            self.q_table = self.q_table.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.q_table.columns,
                    name=state,
                )
            )

    def choose_action(self, observation):
        # First, make sure the state actually has an entry in the table
        self.check_state_exist(observation)

        # Then use epsilon-greedy to select an action.
        # exactly the same as provided asynch pi example
        if np.random.uniform() >= self.epsilon:
            state_action = self.q_table.loc[observation,:]
            action = np.random.choice(state_action[state_action == np.max(state_action)].index)
        else:
            action = np.random.choice(self.actions)
        return action

    def learn(self, s, a, r, s_):
        # SARSA. Update the expected value of the last state by the discounted reward of the future state
        self.check_state_exist(s_)
        if s_ != 'terminal':
            a_ = self.choose_action(str(s_))
            q_target = self.q_table.loc[s,a] + self.lr * \
                (r + self.gamma*self.q_table.loc[s_, a_] - self.q_table.loc[s,a])
        else:
            # s_ is terminal. The expected reward is just the reward - there are no future states.
            q_target = r
            a_ = None

        self.q_table.loc[s,a] = q_target
        return s_, a_

