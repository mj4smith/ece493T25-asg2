import numpy as np
import pandas as pd


class rlalgorithm:
    def __init__(self, actions, learning_rate=0.01, reward_decay=0.9, e_greedy=0.1, trace_decay=0.9):
        self.actions = actions
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.q_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.t_table = dict()
        self.display_name = "SARSA Lambda"
        self.trace = []
        self.lam = trace_decay
        self.trace_limit = 0.01

    '''States are dynamically added to the Q(S,A) table as they are encountered'''
    def check_state_exist(self, state):
        if state not in self.q_table.index:
            # append new state to q table
            self.q_table = self.q_table.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.q_table.columns,
                    name=state,
                )
            )

    def choose_action(self, observation):
        # First, make sure the state actually has an entry in the table
        self.check_state_exist(observation)

        # Then use epsilon-greedy to select an action.
        # exactly the same as provided asynch pi example
        if np.random.uniform() >= self.epsilon:
            state_action = self.q_table.loc[observation,:]
            action = np.random.choice(state_action[state_action == np.max(state_action)].index)
        else:
            action = np.random.choice(self.actions)
        return action

    def learn(self, s, a, r, s_):
        # SARSA Lambda.
        self.check_state_exist(s_)
        clear_list = []

        if s_ != 'terminal':
            a_ = self.choose_action(str(s_))
            d = r + self.gamma*self.q_table.at[str(s_), a_] - self.q_table.at[str(s),a]

            # add 1 to the trace at the current s,a location
            if self.t_table.get((s,a)):
                self.t_table[(s,a)] += 1
            else:
                self.t_table[(s,a)] = 1
            # q_target = r
            # for all s,a pairs in the trace, t_table
            for s_1, a_1 in self.t_table.keys():
                q_target = self.q_table.at[s_1, a_1] + self.lr * (d * self.t_table[(s_1, a_1)])
                self.q_table.at[s_1,a_1] = q_target
                # update trace table
                self.t_table[(s_1, a_1)] = self.gamma * \
                    self.lam * self.t_table[(s_1, a_1)]
                # remove traces that are pointlessly small
                if self.t_table[(s_1, a_1)] < self.trace_limit:
                    clear_list.append((s_1, a_1))
        else:
            # s_ is terminal. The expected reward is just the reward - there are no future states.
            # TODO should I be updating the trace table in the terminal state?
            # Or should I be clearing it? The algorithm block implies keep, but
            # that doesn't make a lot of sense.
            q_target = r
            self.q_table.loc[str(s),a] = r
            d = 0
            for s_1, a_1 in self.t_table.keys():
                q_target = self.q_table.at[str(s_1), a_1] + self.lr * (d * self.t_table[(s_1, a_1)])
                self.q_table.at[str(s_1),a_1] = q_target
                # update trace table
                self.t_table[(s_1, a_1)] = self.gamma * \
                    self.lam * self.t_table[(s_1, a_1)]
                # remove traces that are pointlessly small
                if self.t_table[(s_1, a_1)] < self.trace_limit:
                    clear_list.append((s_1, a_1))

            a_ = None
        for s_1, a_1 in clear_list:
            del self.t_table[(s_1, a_1)]

        return s_, a_

