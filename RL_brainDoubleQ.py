import numpy as np
import pandas as pd


class rlalgorithm:
    def __init__(self, actions, learning_rate=0.01, reward_decay=0.9, e_greedy=0.1):
        self.actions = actions
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.q_tableA = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.q_tableB = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.display_name = "Double Q-Learning"

    '''States are dynamically added to the Q(S,A) table as they are encountered'''
    def check_state_exist(self, state):
        if state not in self.q_tableA.index:
            # append new state to q table
            self.q_tableA = self.q_tableA.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.q_tableA.columns,
                    name=state,
                )
            )
            self.q_tableB = self.q_tableB.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.q_tableB.columns,
                    name=state,
                )
            )

    def choose_action(self, observation):
        # First, make sure the state actually has an entry in the table
        self.check_state_exist(observation)

        # Then use epsilon-greedy to select an action.
        # epsilon greedy in Q1 + Q2
        if np.random.uniform() >= self.epsilon:
            state_actionA = self.q_tableA.loc[observation,:]
            state_actionB = self.q_tableB.loc[observation,:]
            state_action = state_actionA + state_actionB
            action = np.random.choice(state_action[state_action == np.max(state_action)].index)
        else:
            action = np.random.choice(self.actions)
        return action

    def off_policy_action(self, q_table, observation):
        self.check_state_exist(observation)
        state_action = q_table.loc[observation, :]
        action = np.random.choice(state_action[state_action == np.max(state_action)].index)
        return action

    def learn(self, s, a, r, s_):
        # SARSA. Update the expected value of the last state by the discounted reward of the future state
        self.check_state_exist(s_)

        # when we
        if np.random.uniform() >= 0.5:
            q_table = self.q_tableA
            q_table_other = self.q_tableB
        else:
            q_table = self.q_tableB
            q_table_other = self.q_tableA

        if s_ != 'terminal':
            a_m = self.off_policy_action(q_table, str(s_))
            a_ = self.choose_action(str(s_))
            q_target = q_table.loc[s,a] + self.lr * \
                (r + self.gamma*q_table_other.loc[s_, a_m] - q_table.loc[s,a])
        else:
            # s_ is terminal. The expected reward is just the reward - there are no future states.
            q_target = r
            a_ = None

        q_table.loc[s,a] = q_target
        return s_, a_

